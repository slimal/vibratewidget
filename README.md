# VibrateWidget

The widget will make the watch vibrate "n" times if it's "n" a.m./p.m. 
For example, if it's 5:45 a.m., the device would vibrate 5 times. This can be useful if you're asleep and want to know approximately the hour without having to open your eyes.

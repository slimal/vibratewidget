import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
import Toybox.System;
import Toybox.Timer;
import Toybox.Attention;

class VibrateWidgetApp extends Application.AppBase {

	var count = 0;
	var myTimer = new Timer.Timer();
	var vibeData = [new Attention.VibeProfile(50, 1000), new Attention.VibeProfile(0, 500)];
	
    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
       
       if (Attention has :vibrate) {
       	count = System.getClockTime().hour.toNumber() % 12;
    	//System.println("Count : " + count);
    	timerCallback();
		myTimer.start(method(:timerCallback), 2050, true);
		}
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        return [ new VibrateWidgetView() ] as Array<Views or InputDelegates>;
    }

	function timerCallback() {
		if (count == 0){
			myTimer.stop();
			System.exit();
		} else {	
			Attention.vibrate(vibeData);
			count = count-1;
		}
	}
 
}

function getApp() as VibrateWidgetApp {
    return Application.getApp() as VibrateWidgetApp;
}